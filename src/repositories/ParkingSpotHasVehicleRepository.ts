import { EntityRepository, Repository } from "typeorm";
import ParkingSpotHasVehicle from "../models/ParkingSpotHasVehicle";

@EntityRepository(ParkingSpotHasVehicle) 
class ParkingSpotHasVehicleRepository extends Repository<ParkingSpotHasVehicle> {

}
export{ParkingSpotHasVehicleRepository}