export interface Request {
    totalValue: number | undefined,
    diffDays: number,
    timehour: number,
    timemin: number
}

export default function timeCalc(checkIn: Date, checkOut: Date, timeValue: number | undefined, daily?: number, overnightStay?: number): Request {

    let timeDiff = Math.abs(checkOut.getTime() - checkIn.getTime());
    let timehour = Math.abs(checkOut.getHours() - checkIn.getHours());
    let timemin = Number(((timeDiff / 60000) % 60).toFixed(0));
    let totalValue;

    if (checkIn.getMinutes() > checkOut.getMinutes()) {
        timehour--
    }

    let diffDays = Math.ceil((timeDiff / (1000 * 3600 * 24)) - 1);

    // Calculo das horas
    if(timeValue){
    const calcHour = (timehour * timeValue)
    let calcMin
    let calcDay

    // Calculo dos minutos e verificação se for menor que 30 minutos
    if (timemin <= 30) {
        calcMin = timeValue / 2
    } else {
        calcMin = timeValue
    }

    // Calculo dos dias (diária)
    if (diffDays > 0) {
        calcDay = diffDays * daily
    } else {
        calcDay = 0
    }

    // Calculo total do valor a ser pago
    totalValue = calcHour + calcMin + calcDay

}

return {
    totalValue: totalValue,
    diffDays: diffDays,
    timehour: timehour,
    timemin: timemin
}
}
