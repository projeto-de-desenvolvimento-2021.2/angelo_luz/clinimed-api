import { Column, CreateDateColumn, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryColumn, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";

export enum EmployeeRole{
    ADMIN = "admin",
    USER = "user",
}

@Entity("employee")
export default class Employee {

    @PrimaryGeneratedColumn()
    id: number

    @Column()
    name: string

    @Column()
    cpf: string
   
    @Column()
    password: string

    @Column()
    email: string

    @Column()
    birth: Date

    @Column()
    address: string

    @Column({type: 'enum', enum: EmployeeRole, default: EmployeeRole.USER})
    role: EmployeeRole

    @Column()
    phone: string

    @CreateDateColumn({ name: 'Created_at' })
    createdAt: Date

    @UpdateDateColumn({ name: 'Updated_at' })
    updatedAt: Date
}