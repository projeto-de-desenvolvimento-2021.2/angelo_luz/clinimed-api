import {
    Column,
    CreateDateColumn,
    Entity,
    PrimaryGeneratedColumn,
    UpdateDateColumn,
    ManyToOne, 
    JoinColumn
} from "typeorm";

import Employee from "./Employee";

@Entity("cashier")
export default class Cashier {

    @PrimaryGeneratedColumn()
    id: number;

    @Column("float8", { name: 'starting_amount', nullable: true })
    startingAmount: number

    @Column("float8", { name: 'ending_amount', nullable: true })
    endingAmount: number

    @Column("float8", { name: 'ending_amout_user', nullable: true })
    endingAmountUser: number

    @Column({ name: 'start_date', nullable: true })
    startDate: Date

    @Column({ name: 'end_date', nullable: true })
    endDate: Date

    @Column("float8", { name: 'count_amount', nullable: true })
    countAmount: number

    @ManyToOne(() => Employee, { nullable: true })
    @JoinColumn({ name: 'employee_id' })
    employeeId: Employee;

    @CreateDateColumn({ name: 'Created_at' })
    createdAt: Date

    @UpdateDateColumn({ name: 'Updated_at' })
    updatedAt: Date
}