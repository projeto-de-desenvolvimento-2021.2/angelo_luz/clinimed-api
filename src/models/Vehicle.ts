import {
    Column,
    CreateDateColumn,
    Entity,
    ManyToOne,
    PrimaryGeneratedColumn,
    UpdateDateColumn,
    JoinColumn
} from "typeorm";

@Entity("vehicle")
export default class Vehicle {

    @PrimaryGeneratedColumn()
    id: number

    @Column()
    model: string

    @Column()
    color: string

    @Column()
    license: string

    @Column()
    category: string

    @Column()
    typeVehicle: string

    @CreateDateColumn({ name: 'Created_at' })
    createdAt: Date

    @UpdateDateColumn({ name: 'Updated_at' })
    updatedAt: Date
}