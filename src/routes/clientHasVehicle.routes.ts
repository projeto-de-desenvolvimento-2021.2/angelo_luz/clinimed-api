import { Router } from 'express'
import ClientHasVehicleController from '../controllers/ClientHasVehicleController'

const ClientHasVehicleRouter = Router()
const clientHasVehicleController = new ClientHasVehicleController()


ClientHasVehicleRouter.post("/", clientHasVehicleController.create)
 
ClientHasVehicleRouter.get("/", clientHasVehicleController.list)

ClientHasVehicleRouter.get("/getClientsByVehicle/:id", clientHasVehicleController.getClientsByVehicleId)

ClientHasVehicleRouter.get("/getVehiclesByClient/:id", clientHasVehicleController.getVehiclesByClientId)

ClientHasVehicleRouter.delete("/:id", clientHasVehicleController.delete)

ClientHasVehicleRouter.put("/:id", clientHasVehicleController.update)


export default ClientHasVehicleRouter;