import { Router } from 'express'
import OutputCashierController from '../controllers/OutputCashierController'
import { authorization } from '../middlewares/authorization'

const outputCashierRouter = Router()
const outputCashierController = new OutputCashierController()

outputCashierRouter.get("/getOutputByCashierId/:cashierId", outputCashierController.getOutputsByCashierId)
outputCashierRouter.get("/getInputAndOutputsByCashierId/:cashierId", outputCashierController.getInputAndOutputsByCashierId)
outputCashierRouter.get("/getAllInputsAndOutputs", outputCashierController.getAllInputsAndOutputs)

outputCashierRouter.use(authorization)

outputCashierRouter.post("/:employeeId", outputCashierController.create)
outputCashierRouter.delete("/:id", outputCashierController.delete)

export default outputCashierRouter