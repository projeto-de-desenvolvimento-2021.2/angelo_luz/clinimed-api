import { Router } from 'express'
import ClientController from '../controllers/ClientController'
import { authorization } from '../middlewares/authorization'

const clientRouter = Router()
const clientController = new ClientController()


clientRouter.post("/", clientController.create)
 
clientRouter.get("/", authorization, clientController.list)

clientRouter.get("/:id", authorization, clientController.getById)

clientRouter.get("/get/name", clientController.getByName)

clientRouter.delete("/:id", clientController.delete)

clientRouter.put("/:id", clientController.update)


export default clientRouter