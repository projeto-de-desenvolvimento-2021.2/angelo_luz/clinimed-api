import { Request, Response } from "express"

import ParkingSpotHasVehicleService from '../services/ParkingSpotHasVehicleService'
const parkingSpotHasVehicleService = new ParkingSpotHasVehicleService()

export default class ParkingSpotHasVehicleController {
    async create(req: Request, res: Response) {
        const parkingSpotHasVehicle = await parkingSpotHasVehicleService.create(req.body)
        return res.status(201).json(parkingSpotHasVehicle)
    }

    async listAll(req: Request, res: Response) {
        const listAll = await parkingSpotHasVehicleService.listAll()
        return res.status(200).json(listAll)
    }

    async list(req: Request, res: Response) {
        const listAll = await parkingSpotHasVehicleService.list()
        return res.status(200).json(listAll)
    }

    async getById(req: Request, res: Response) {
        const parkingSpotHasVehicle = await parkingSpotHasVehicleService.getById(Number(req.params.id))
        return res.status(200).json(parkingSpotHasVehicle)
    }

    async amountToPay(req: Request, res: Response) {
        const parkingSpotHasVehicle = await parkingSpotHasVehicleService.amountToPay(Number(req.params.id))
        return res.status(200).json(parkingSpotHasVehicle)
    }

    async delete(req: Request, res: Response) {
        await parkingSpotHasVehicleService.delete(Number(req.params.id))
        return res.status(200).json('Veículo excluido com sucesso!')
    }

    async update(req: Request, res: Response) {
        const parkingSpotHasVehicleUpdate = await parkingSpotHasVehicleService.Update(Number(req.params.id), req.body)
        return res.status(200).json(parkingSpotHasVehicleUpdate)
    }

    async leave(req: Request, res: Response) {
        const parkingSpotHasVehicleUpdate = await parkingSpotHasVehicleService.leave(Number(req.params.id), Number(req.params.employeeId))
        return res.status(200).json(parkingSpotHasVehicleUpdate)
    }
}
