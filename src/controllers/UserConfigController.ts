import { Request, Response } from "express"

import UserConfigService from '../services/UserConfigService'
const userConfigService = new UserConfigService()

export default class UserConfigController {
    async create(req: Request, res: Response) {
        const userConfig = await userConfigService.create(req.body)
        return res.status(201).json(userConfig)
    }

    async list(req: Request, res: Response) {
        const listAll = await userConfigService.list()
        return res.status(200).json(listAll)
    }

    async getById(req: Request, res: Response) {
        const userConfig = await userConfigService.getById(Number(req.params.id))
        return res.status(200).json(userConfig)
    }

    async delete(req: Request, res: Response) {
        await userConfigService.delete(Number(req.params.id))
        return res.status(200).json('Funcionário excluido com sucesso!')
    }

    async update(req: Request, res: Response) {
        const userConfigUpdate = await userConfigService.Update(Number(req.params.id), req.body)
        return res.status(200).json(userConfigUpdate)
    }
}
