import { Request, Response } from "express"

import ParkingSpotService from '../services/ParkingSpotService'
const parkingSpotService = new ParkingSpotService()

export default class ParkingSpotController {
    async create(req: Request, res: Response) {
        const parkingSpot = await parkingSpotService.CreateSpotsDinamic(req.body)
        return res.status(201).json(parkingSpot)
    }

    async list(req: Request, res: Response) {
        const listAll = await parkingSpotService.list()
        return res.status(200).json(listAll)
    }

    async getById(req: Request, res: Response) {
        const parkingSpot = await parkingSpotService.getById(Number(req.params.id))
        return res.status(200).json(parkingSpot)
    }

    async delete(req: Request, res: Response) {
        await parkingSpotService.delete(Number(req.params.id))
        return res.status(200).json('Vaga excluida com sucesso!')
    }

    async update(req: Request, res: Response) {
        const parkingSpotUpdate = await parkingSpotService.Update(Number(req.params.id), req.body)
        return res.status(200).json(parkingSpotUpdate)
    }
}
