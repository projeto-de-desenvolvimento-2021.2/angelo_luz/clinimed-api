import { Request, Response } from "express"

import SpotValueService from '../services/SpotValueService'
const spotValueService = new SpotValueService()

export default class SpotValueController {
    async create(req: Request, res: Response) {
        const spotValue = await spotValueService.create(req.body)
        return res.status(201).json(spotValue)
    }

    async list(req: Request, res: Response) {
        const listAll = await spotValueService.list()
        return res.status(200).json(listAll)
    }

    async getById(req: Request, res: Response) {
        const spotValue = await spotValueService.getById(Number(req.params.id))
        return res.status(200).json(spotValue)
    }

    async delete(req: Request, res: Response) {
        await spotValueService.delete(Number(req.params.id))
        return res.status(200).json('Vaga excluida com sucesso!')
    }

    async update(req: Request, res: Response) {
        const spotValueUpdate = await spotValueService.Update(Number(req.params.id), req.body)
        return res.status(200).json(spotValueUpdate)
    }
}
