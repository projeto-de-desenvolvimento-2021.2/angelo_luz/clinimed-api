import { Request, Response } from "express"

import VehicleService from '../services/VehicleService'
const vehicleService = new VehicleService()
export default class VehicleController {
    
    async create(req: Request, res: Response) {
        const vehicle = await vehicleService.create(req.body)
        return res.status(201).json(vehicle)
    }

    async list(req: Request, res: Response) {
        const listAll = await vehicleService.list()
        return res.status(200).json(listAll)
    }

    async getById(req: Request, res: Response) {
        const vehicle = await vehicleService.getById(Number(req.params.id))
        return res.status(200).json(vehicle)
    }

    async getBylicense(req: Request, res: Response) {
        const vehicle = await vehicleService.getBylicense(req.params.license)
        return res.status(200).json(vehicle)
    }

    async delete(req: Request, res: Response) {
        await vehicleService.delete(Number(req.params.id))
        return res.status(200).json('Veículo excluido com sucesso!')
    }

    async update(req: Request, res: Response) {
        const vehicleUpdate = await vehicleService.Update(Number(req.params.id), req.body)
        return res.status(200).json(vehicleUpdate)
    }
}


