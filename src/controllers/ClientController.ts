import { Request, Response } from "express"

import ClientService from '../services/ClientService'
const clientService = new ClientService()

export default class ClientController {
    async create(req: Request, res: Response) {
            const client = await clientService.create(req.body)
            return res.status(201).json(client)
    }

    async list(req: Request, res: Response) {
            const listAll = await clientService.list()
            return res.status(200).json(listAll)
    }

    async getByName(req: Request, res: Response) {
            const client = await clientService.getByName(req.body.name)
            return res.status(200).json(client)
    }

    async getById(req: Request, res: Response) {
            const client = await clientService.getById(Number(req.params.id))
            return res.status(200).json(client)
    }

    async delete(req: Request, res: Response) {
            await clientService.delete(Number(req.params.id))
            return res.status(200).json('Cliente excluido com sucesso!')
    }

    async update(req: Request, res: Response) {
            const clientUpdate = await clientService.Update(Number(req.params.id), req.body)
            return res.status(200).json(clientUpdate)
    }
}
