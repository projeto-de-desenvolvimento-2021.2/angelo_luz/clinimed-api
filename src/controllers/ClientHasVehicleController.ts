import { Request, Response } from "express"

import CLientHasVehicleService from '../services/ClientHasVehicleService'
const clientHasVehicleService = new CLientHasVehicleService()

export default class ParkingSpotHasVehicleController {
    async create(req: Request, res: Response) {
        const clientHasVehicle = await clientHasVehicleService.create(req.body)
        return res.status(201).json(clientHasVehicle)
    }

    async list(req: Request, res: Response) {
        const listAll = await clientHasVehicleService.list()
        return res.status(200).json(listAll)
    }

    async getClientsByVehicleId(req: Request, res: Response) {
        const clientHasVehicle = await clientHasVehicleService.getClientsByVehicleId(Number(req.params.id))
        return res.status(200).json(clientHasVehicle)
    }

    async getVehiclesByClientId(req: Request, res: Response) {
        const clientHasVehicle = await clientHasVehicleService.getVehiclesByClientId(Number(req.params.id))
        return res.status(200).json(clientHasVehicle)
    }

    async delete(req: Request, res: Response) {
        await clientHasVehicleService.delete(Number(req.params.id))
        return res.status(200).json('Veículo excluido com sucesso!')
    }

    async update(req: Request, res: Response) {
        const clientHasVehicle = await clientHasVehicleService.Update(Number(req.params.id), req.body)
        return res.status(200).json(clientHasVehicle)
    }
}
