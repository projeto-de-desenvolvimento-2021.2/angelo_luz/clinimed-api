import { Request, Response } from "express"

import EmployeeService from '../services/EmployeeService'
const employeeService = new EmployeeService()

export default class EmployeeController {
    async create(req: Request, res: Response) {
            const employee = await employeeService.create(req.body)
            return res.status(201).json(employee)
    }

    async list(req: Request, res: Response) {
            const listAll = await employeeService.list()
            return res.status(200).json(listAll)       
    }

    async getById(req: Request, res: Response) {
            const employee = await employeeService.getById(Number(req.params.id))
            return res.status(200).json(employee)
    }
   
    async getByName(req: Request, res: Response) {
            const employee = await employeeService.getByName(req.body.name)
            return res.status(200).json(employee)
    }

    async getByCpf(req: Request, res: Response) {
            const employee = await employeeService.getByCpf(req.body.cpf)
            return res.status(200).json(employee)
    }

    async login(req: Request, res: Response) {
        const employee = await employeeService.login(req.body)
        return res.status(200).json(employee)
    }

    async delete(req: Request, res: Response) {
            await employeeService.delete(Number(req.params.id))
            return res.status(200).json('Funcionário excluido com sucesso!')            
    }

    async update(req: Request, res: Response) {
            const employeeUpdate = await employeeService.Update(Number(req.params.id), req.body)
            return res.status(200).json(employeeUpdate)
    }
}
