import app from './app'

require('dotenv').config();

app.listen(process.env.API_PORT, () => {
    console.log(`Server Running on Port ${process.env.API_PORT}`);
});