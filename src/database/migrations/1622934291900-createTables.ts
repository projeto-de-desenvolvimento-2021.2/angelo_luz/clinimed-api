import {MigrationInterface, QueryRunner} from "typeorm";

export class createTables1622934291900 implements MigrationInterface {
    name = 'createTables1622934291900'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "client" ("id" SERIAL NOT NULL, "name" character varying NOT NULL, "phone" character varying NOT NULL, "email" character varying NOT NULL, "address" character varying NOT NULL, "birth" TIMESTAMP NOT NULL, "cpf_cnpj" character varying NOT NULL, "pay_day" TIMESTAMP NOT NULL, "status" boolean NOT NULL, "Created_at" TIMESTAMP NOT NULL DEFAULT now(), "Updated_at" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "PK_96da49381769303a6515a8785c7" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "employee" ("id" SERIAL NOT NULL, "name" character varying NOT NULL, "cpf" character varying NOT NULL, "password" character varying NOT NULL, "email" character varying NOT NULL, "birth" TIMESTAMP NOT NULL, "address" character varying NOT NULL, "admin" boolean NOT NULL, "phone" character varying NOT NULL, "Created_at" TIMESTAMP NOT NULL DEFAULT now(), "Updated_at" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "PK_3c2bc72f03fd5abbbc5ac169498" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "userConfig" ("id" SERIAL NOT NULL, "report" boolean NOT NULL, "register" boolean NOT NULL, "vehicle_update" boolean NOT NULL, "Created_at" TIMESTAMP NOT NULL DEFAULT now(), "Updated_at" TIMESTAMP NOT NULL DEFAULT now(), "employee_id" integer, CONSTRAINT "REL_29d001417c49f5ceffd16ce92f" UNIQUE ("employee_id"), CONSTRAINT "PK_899f5b5673382cfc66c43af7464" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "vehicle" ("id" SERIAL NOT NULL, "model" character varying NOT NULL, "color" character varying NOT NULL, "license" character varying NOT NULL, "category" character varying NOT NULL, "typeVehicle" character varying NOT NULL, "Created_at" TIMESTAMP NOT NULL DEFAULT now(), "Updated_at" TIMESTAMP NOT NULL DEFAULT now(), "client_id" integer, CONSTRAINT "PK_187fa17ba39d367e5604b3d1ec9" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "userConfig" ADD CONSTRAINT "FK_29d001417c49f5ceffd16ce92f9" FOREIGN KEY ("employee_id") REFERENCES "employee"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "vehicle" ADD CONSTRAINT "FK_c23a177d7fec265e8ca41f4edb0" FOREIGN KEY ("client_id") REFERENCES "client"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "vehicle" DROP CONSTRAINT "FK_c23a177d7fec265e8ca41f4edb0"`);
        await queryRunner.query(`ALTER TABLE "userConfig" DROP CONSTRAINT "FK_29d001417c49f5ceffd16ce92f9"`);
        await queryRunner.query(`DROP TABLE "vehicle"`);
        await queryRunner.query(`DROP TABLE "userConfig"`);
        await queryRunner.query(`DROP TABLE "employee"`);
        await queryRunner.query(`DROP TABLE "client"`);
    }

}
