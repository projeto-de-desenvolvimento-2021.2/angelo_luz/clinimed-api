import { EmployeeRepository } from '../repositories/EmployeeRepository'
import UserConfigService from '../services/UserConfigService'
import { getCustomRepository, Like } from "typeorm"
import { hashed, comparePassword } from '../utils/password'
import { employeeTokenGenerator } from '../utils/jwt'
import { EmployeeRole } from '../models/Employee'

import AppError from '../errors/Errors'

interface Request {
    name: string
    password: string
    email: string
    birth: Date
    cpf: string
    address: string
    role: EmployeeRole
    phone: string
}

interface ILogin {
    email: string
    password: string
}

export default class EmployeeService {

    async create({ name, password, email, birth, cpf, address, role, phone }: Request) {
        const employeeRepository = getCustomRepository(EmployeeRepository)

        const userConfigService = new UserConfigService()

        const employeeAlreadyExists = await employeeRepository.findOne({
            cpf
        })

        if (employeeAlreadyExists) {
            throw new AppError('Funcionário já existente')
        }

        const encrypted = await hashed(password)

        const employee = employeeRepository.create({
            name: name.toUpperCase(),
            password: encrypted,
            email,
            birth,
            cpf,
            address,
            role,
            phone,
        })
        
        await employeeRepository.save(employee)

        if(employee.role === 'admin') {
            userConfigService.create({ 
                employeeConfigId: employee.id, 
                register: true, 
                report: true, 
                vehicleUpdate: true 
            })
        }
        else {
            userConfigService.create({ 
                employeeConfigId: employee.id, 
                register: false, 
                report: false, 
                vehicleUpdate: false 
            })
        }

        delete employee.password

        return employee
    }

    async list() {
        const employeeRepository = getCustomRepository(EmployeeRepository)

        const all = await employeeRepository.find()

        return all
    }

    async getById(id: number) {
        
        const employeeRepository = getCustomRepository(EmployeeRepository)

        const employee = await employeeRepository.findOne(id)

        if (!employee) {
            throw new AppError('Funcionário não encontrado!')
        }

        return employee
    }

    async login({ email, password }: ILogin) {
        const employeeRepository = getCustomRepository(EmployeeRepository)
        const employee = await employeeRepository.findOne({
            email
        })
        
        if (!employee){
            throw new AppError('Email ou senha incorreta', 401)
        }

        const isTrue = await comparePassword(password, employee.password)

        if(!isTrue) {
            throw new AppError('Email ou senha incorreta', 401)
        }

        const token = employeeTokenGenerator(employee)

        delete employee.password
        
        return {token, employee}
    }

    async getByName(name: string) {
        const employeeRepository = getCustomRepository(EmployeeRepository)

        const employee = await employeeRepository.find({ name: Like(`%${name.toUpperCase()}%`) })
        
        if (!employee) {
            throw new AppError('Funcionário não encontrado!')
        }

        return employee
    }
    
    async getByCpf(cpf: string) {

        const employeeRepository = getCustomRepository(EmployeeRepository)

        const employee = await employeeRepository.find({ cpf: Like(`%${cpf}%`) })

        if (!employee) {
            throw new AppError('Funcionário não encontrado!')
        }

        return employee
    }

    async delete(id: number) {
        const employeeRepository = getCustomRepository(EmployeeRepository)

        const employee = await employeeRepository.findOne(id)

        if (!employee) {
            throw new AppError('Funcionário não encontrado!')
        }

        const deleted = await employeeRepository.delete(id)

        return deleted
    }

    async Update(id: number, { name, password, phone, email, birth, cpf, address, role }: Request) {

        const employeeRepository = getCustomRepository(EmployeeRepository)

        const employee = await employeeRepository.findOne(id)

        if (!employee) {
            throw new AppError('Funcionário não encontrado!')
        }

        const encrypted = await hashed(password)

        await employeeRepository.update(id, {
            name, password: encrypted, phone, email, birth, cpf, address, role
        })

        const employeeUpdate = await employeeRepository.findOne(id)

        return employeeUpdate
    }

}

