import { AmountSpotRepository } from '../repositories/AmountSpotRepository'
import { getCustomRepository } from "typeorm"

import AppError from '../errors/Errors'

export interface Request {
    amountSpot: number
}

export default class AmountSpotService {

    async create({ amountSpot }: Request) {

        const amountSpotRepository = getCustomRepository(AmountSpotRepository)

        const amountSpotCreate = amountSpotRepository.create({
          amountSpot
        })

        await amountSpotRepository.save(amountSpotCreate)
        return amountSpotCreate
    }

    async list() {
        const amountSpotRepository = getCustomRepository(AmountSpotRepository)

        const all = await amountSpotRepository.find()

        return all
    }

    async Update(id: number, { amountSpot }: Request) {
        const amountSpotRepository = getCustomRepository(AmountSpotRepository)

        const findAmountSpot = await amountSpotRepository.findOne(id)

        if (!findAmountSpot) {
            throw new AppError('Cliente não encontrado!')
        }

        await amountSpotRepository.update(id, {
            amountSpot
        })

        const amountSpotUpdate = await amountSpotRepository.findOne(id)

        return amountSpotUpdate
    }

}

