import { VehicleRepository } from '../repositories/VehicleRepository'
import { ParkingSpotHasVehicleRepository } from '../repositories/ParkingSpotHasVehicleRepository'
import { ParkingSpotRepository } from '../repositories/ParkingSpotRepository'
import { getCustomRepository } from "typeorm"

import { IRequest } from '../services/ParkingSpotService'
import AppError from '../errors/Errors'

import SpotValueService from '../services/SpotValueService'
const spotValueService = new SpotValueService()

import ParkingSpotServices from '../services/ParkingSpotService'
const parkingSpotServices = new ParkingSpotServices()
interface Request {
    model: string
    color: string
    license: string
    category: string
    typeVehicle: string
}
export default class VehicleService {

    async create({ model, color, license, category, typeVehicle }: Request) {
        const vehicleRepository = getCustomRepository(VehicleRepository)
        const parkingSpotHasVehicleRepository = getCustomRepository(ParkingSpotHasVehicleRepository)
        const parkingSpotRepository = getCustomRepository(ParkingSpotRepository)

        if (!license || !model || !color) {
            throw new AppError('Falta dados no veículo!')
        }

        const vehicleAlreadyExists = await vehicleRepository.findOne({
            license
        })

       let vehicle = vehicleRepository.create({
            model,
            color,
            license,
            category,
            typeVehicle
        })

        if (!vehicleAlreadyExists) { await vehicleRepository.save(vehicle) }

        if(vehicleAlreadyExists){ vehicle = vehicleAlreadyExists }

        const parkingSpotHasVehicle = await parkingSpotHasVehicleRepository.find({ 
            vehicleId: vehicle
        })

        const vehicleStatus = parkingSpotHasVehicle.filter(element =>{ return element.status })
        if(vehicleStatus.length > 0){
            throw new AppError('Veiculo já esta na vaga!')
        }

        const spotValueId = await spotValueService.getByType(typeVehicle)
        
        const data: IRequest = {
            name: "Generic",
            spotValueId: spotValueId
        }
     
        const parkingSpot = await parkingSpotServices.create(data)

        await parkingSpotRepository.save(parkingSpot)

        const parkingSpotHasVehicleCreate = parkingSpotHasVehicleRepository.create({
            parkingSpotId: parkingSpot,
            vehicleId: vehicle,
            checkIn: new Date(),
            status: true,
        })

        await parkingSpotHasVehicleRepository.save(parkingSpotHasVehicleCreate)
        return vehicle
    }

    async list() {
        const vehicleRepository = getCustomRepository(VehicleRepository)

        const all = await vehicleRepository.find()

        return all
    }

    async getBylicense(license: string) {

        const vehicleRepository = getCustomRepository(VehicleRepository)

        const vehicle = await vehicleRepository.findOne({ license })

        if (!vehicle) {
            throw new AppError('Veículo não encontrado!')
        }

        return vehicle
    }

    async getById(id: number) {
        const vehicleRepository = getCustomRepository(VehicleRepository)

        const vehicle = await vehicleRepository.findOne(id)

        if (!vehicle) {
            throw new AppError('Veículo não encontrado!')
        }

        return vehicle
    }

    async delete(id: number) {
        const vehicleRepository = getCustomRepository(VehicleRepository)

        const vehicle = await vehicleRepository.findOne(id)

        if (!vehicle) {
            throw new AppError('Veículo não encontrado!')
        }

        const deleted = await vehicleRepository.delete(id)

        return deleted
    }

    async Update(id: number, { model, color, license, category, typeVehicle }: Request) {
        const vehicleRepository = getCustomRepository(VehicleRepository)

        const vehicle = await vehicleRepository.findOne(id)

        if (!vehicle) {
            throw new AppError('Veículo não encontrado!')
        }

        const licenseAreadyExists = await vehicleRepository.findOne({ license })


        if (license === vehicle.license || !licenseAreadyExists) {
            await vehicleRepository.update(id, {
                model, color, license, category, typeVehicle
            })

            const vehicleUpdate = await vehicleRepository.findOne(id)

            return vehicleUpdate
        }
        throw new AppError("Veículo já cadastrado");
    }
}
