import { OutputCashierRepository } from '../repositories/OutputCashierRepository';
import { InputCashierRepository } from '../repositories/InputCashierRepository';
import { DeleteResult, getCustomRepository } from 'typeorm';
import Cashier from '../models/Cashier';
import OutputCashier from '../models/OutputCashier';

import CashierService from './CashierService';
const cashierService = new CashierService();

import InputCashierService from '../services/InputCashierService';
const inputCashierService = new InputCashierService();

import AppError from '../errors/Errors';

interface ICreateDTO {
  description: string;
  value: number;
  cashierId: Cashier | number;
}

interface IMethods {
  create(employeeId: number , { description, value, cashierId }: ICreateDTO): Promise<OutputCashier>;
  save(inputCashier: OutputCashier): Promise<void>;
  getOutputsByCashierId(cashierId: number): Promise<OutputCashier[]>;
  getInputAndOutputsByCashierId(cashierId: number): Promise<Object>
  getAllInputsAndOutputs(): Promise<Object>
  delete(id: number): Promise<DeleteResult>;
}

export default class OutputCashierService implements IMethods {
    async create(employeeId: number, { description, value, cashierId }: ICreateDTO): Promise<OutputCashier> {
        const outputCashierRepository = getCustomRepository(OutputCashierRepository);
    
        if (!employeeId) throw new AppError('Informe o funcionário!');
        if (!description) throw new AppError('Informe uma descrição!');
         
        const currentcashierId = await cashierService.getCashierByEmployeeId(employeeId)
    
        if(!currentcashierId.length) { throw new AppError("Você não tem nenhum caixa aberto!")}
    
        const outputCashier = outputCashierRepository.create({
          description,
          value,
          cashierId: currentcashierId[0],
        });
    
       await inputCashierService.cashierCountAmountsubtraction(Number(currentcashierId[0]), value)
       this.save(outputCashier);

        return outputCashier;
      }

      async save(outputCashier: OutputCashier): Promise<void> {
        const outputCashierRepository = getCustomRepository(OutputCashierRepository);
        await outputCashierRepository.save(outputCashier);
      }

      async getOutputsByCashierId(cashierId: number): Promise<OutputCashier[]> {
        const outputCashierRepository = getCustomRepository(OutputCashierRepository);
        const alloutputCashier = await outputCashierRepository.find({
          where: [{ cashierId }],
          relations: ['cashierId'],
          order: { id: 'ASC' },
        });
    
        if (!alloutputCashier.length)
          throw new AppError(
            'Não tem saidas para esse caixa, ou caixa inexistente!',
          );
        return alloutputCashier;
      }

      async getInputAndOutputsByCashierId(cashierId: number): Promise<Object> {
        const outputCashierRepository = getCustomRepository(OutputCashierRepository);
        const inputCashierRepository = getCustomRepository(InputCashierRepository);

        const allInputCashier = await inputCashierRepository.find({
          where: [{ cashierId }],
          order: { id: 'ASC' },
        });

        const allOutputCashier = await outputCashierRepository.find({
          where: [{ cashierId }],
          order: { id: 'ASC' },
        });

        if(!allInputCashier.length  && !allOutputCashier.length) throw new AppError("Ainda não existe nenhuma entrada ou saída para esse caixa!")

        const allInputAndOutput = { Inputs: allInputCashier, Outputs: allOutputCashier }

        return allInputAndOutput
      }

      async getAllInputsAndOutputs(): Promise<Object> {
        const outputCashierRepository = getCustomRepository(OutputCashierRepository);
        const inputCashierRepository = getCustomRepository(InputCashierRepository);

        const allInputCashier = await inputCashierRepository.find({
          order: { id: 'ASC' },
        });

        const allOutputCashier = await outputCashierRepository.find({
          order: { id: 'ASC' },
        });

        if(!allInputCashier.length  && !allOutputCashier.length) throw new AppError("Ainda não existe nenhuma entrada ou saída!")

        const allInputAndOutput = { Inputs: allInputCashier, Outputs: allOutputCashier }

        return allInputAndOutput
      }

      async delete(id: number): Promise<DeleteResult> {
        const outputCashierRepository = getCustomRepository(OutputCashierRepository)
  
    
        const outputCashier = await outputCashierRepository.findOne(id,{
          relations: ['cashierId']
        })
    
        if (!outputCashier) throw new AppError('Saída não encontrada!')

        const objInputCashier = Object.assign(Object(outputCashier.cashierId))
    
        const isTrue = await inputCashierService.cashierCountAmountSum(objInputCashier.id, outputCashier.value)
        if(!isTrue) throw new AppError('Algo deu errado, não foi creditado o valor do montante do caixa.')
    
        const deleted = await outputCashierRepository.delete(id)
    
        return deleted
      }
}
