import { ParkingSpotRepository } from '../repositories/ParkingSpotRepository'
import { SpotValueRepository } from '../repositories/SpotValueRepository'
import { getCustomRepository } from "typeorm"

import AppError from '../errors/Errors'
import SpotValue from '../models/SpotValue'

export interface IRequest {
    name: string
    spotValueId: SpotValue | number
    total?: number
}
export default class ParkingSpotService {

    async create({ name, spotValueId }: IRequest) {

        const parkingSpotRepository = getCustomRepository(ParkingSpotRepository)

        const parkingSpot = parkingSpotRepository.create({
            name,
            spotValueId,
        })

        await parkingSpotRepository.save(parkingSpot)
        return parkingSpot
    }

    async CreateSpotsDinamic({ name, spotValueId, total }: IRequest) {
        let obj: string 
        const parkingSpotRepository = getCustomRepository(ParkingSpotRepository)

        for(let i = 1; i <= total; i++) {

             obj = name+' '+i

        const parkingSpot = parkingSpotRepository.create({
            name: obj,
            spotValueId,
        })

        await parkingSpotRepository.save(parkingSpot)
        }

        return "Vagas criadas com sucesso!"
    }

    async list() {
        const parkingSpotRepository = getCustomRepository(ParkingSpotRepository)

        const all = await parkingSpotRepository.find()

        return all
    }

    async getById(id: number) {

        const parkingSpotRepository = getCustomRepository(ParkingSpotRepository)
        const parkingSpot = await parkingSpotRepository.findOne(id, { relations: ["spotValueId"] })

        if (!parkingSpot) {
            throw new AppError('Vaga não encontrada!')
        }

        return parkingSpot
    }

    async delete(id: number) {
        const parkingSpotRepository = getCustomRepository(ParkingSpotRepository)

        const parkingSpot = await parkingSpotRepository.findOne(id)

        if (!parkingSpot) {
            throw new AppError('Vaga não encontrada!')
        }

        const deleted = await parkingSpotRepository.delete(id)

        return deleted
    }

    async Update(id: number, { name, spotValueId }: IRequest) {
        const parkingSpotRepository = getCustomRepository(ParkingSpotRepository)

        const parkingSpot = await parkingSpotRepository.findOne(id)

        if (!parkingSpot) {
            throw new AppError('Vaga não encontrada!')
        }

        await parkingSpotRepository.update(id, {
            name,
            spotValueId
        })

        const parkingSpotUpdate = await parkingSpotRepository.findOne(id)

        return parkingSpotUpdate
    }
}


