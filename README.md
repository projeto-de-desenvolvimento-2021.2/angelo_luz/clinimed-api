# SGE - Sistema de Gerenciamento de Estacionamentos
<br>
<h1>🖌️ Authors</h1>


<ul>
  <li><a href="https://gitlab.com/wagnerandersson">Wagner Andersson</a></li>
  <li><a href="https://gitlab.com/W-araujo"">Wesley Araujo</a></li>
</ul>

<h1> ➡ Contents </h1>
<ul>
    <li>
    <a href="#technologies">Technologies</a>
    </li>
    <li>
      <a href="#intallation">Installation</a>
    </li>
    <li>
     <a href="#getting-started">Getting Started</a>
    </li>
</ul>

<h1 href="#technologies">⬇ Technologies</h1>

<h4>Back-end</h4>
<ul>
<li>Typescript</li>
<li>Express</li>
<li>TypeORM</li>
<li>Docker</li>
<li>Postgres</li>
<li>Redis</li>
</ul>
    
<h1 href="#installation">🔧 Installation </h1>
<h4>You need to install Node.js and Yarn first and then, to clone the project with HTTPS, run this command:</h4>

<p><code>git clone https://gitlab.com/projeto-de-desenvolvimento-2021.2/angelo_luz/clinimed-api.git</code></p>

<h4>Or to clone project with SSH, run this command:</h4>

<p><code>git clone git@gitlab.com:projeto-de-desenvolvimento-2021.2/angelo_luz/clinimed-api.git</code></p>

<h4>(Back-end) Install all dependecies</h4>

<p><code>npm install</code></p>
<h5>Or</h5>
<p><code>yarn install</code></p>

<h4>Need to have the docker installed, at this link: <a href="https://www.docker.com/get-started">Docker Install</a></h4>


<h1 href="#getting-started">💻 Getting Started </h1>

  <ul>
        <li>
           Create your enviroment variables based on the examples of .env.example
        </li>
        <li>
           Fill in all fields according to your database
        </li>
        <li>
            Run this command to upload the application and create the tables <code>docker-compose up</code>
        </li>
   </ul>


    
 
